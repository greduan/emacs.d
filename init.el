(add-to-list 'load-path "~/.emacs.d/lisp")

(require 'my-packages)
(require 'endless-utils)

;; Dired - jumps up one dir
(define-key dired-mode-map (kbd "-") 'dired-up-directory)

;; Disable splash screen
(setq inhibit-splash-screen t)

;; Disable GUI stuff
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)

;; Modeline
(column-number-mode 1)
(display-time-mode 1)
(setq uniquify-buffer-name-style 'forward
      uniquify-strip-common-suffix nil)

;; Changes all yes/no questions to y/n type
(fset 'yes-or-no-p 'y-or-n-p)

;; Use spaces, tabs not
(set-default 'indent-tabs-mode nil)

;; Stop littering everywhere with save files, put them somewhere
(setq backup-directory-alist '(("." . "~/.emacs-backups")))

;; Font
(set-default-font "luculent 9")

;; Disable soft-wrapping
(set-default 'truncate-lines t)
(setq truncate-partial-width-windows 1)

;; Delete trailing whitespace on save hook
(add-hook 'before-save-hook 'delete-trailing-whitespace)

;; Disable cursor blinking
(blink-cursor-mode 0)

;; Set *scratch* to be org-mode
(setq initial-major-mode 'org-mode)

;; Empty *scratch*
(setq initial-scratch-message nil)

;; Default fill-column to 80
(setq-default fill-column 80)
