;; Bootstrap use-package, i.e. install if not installed
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; use-package
(require 'use-package)

(use-package evil-leader
  :ensure t
  :config
  (progn
    (evil-leader/set-leader "<SPC>")
    (evil-leader/set-key
      "b" 'switch-to-buffer
      "p" 'projectile-find-file
      "m" 'magit-status
      "f" 'avy-goto-word-0)
    (global-evil-leader-mode 1)))

(use-package evil
  :ensure t
  :init
  (setq evil-want-fine-undo 'fine ;; more Vim-like undo
        evil-echo-state nil
        evil-search-module 'evil-search
        evil-mode-line-format 'before)
  :config
  (progn
    (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
    (define-key evil-normal-state-map (kbd "-") 'dired-jump)
    (define-key evil-normal-state-map (kbd "s") 'embrace-commander)
    (define-key evil-normal-state-map (kbd "C-w C-w") 'switch-window)
    ;; Enable use of gn motion/text-object
    (define-key evil-normal-state-map (kbd "/") 'evil-ex-search-forward)
    (define-key evil-normal-state-map (kbd "?") 'evil-ex-search-backward)
    (define-key evil-normal-state-map (kbd "n") 'evil-ex-search-next)
    (define-key evil-normal-state-map (kbd "N") 'evil-ex-search-previous)
    ;; Normal paragraph binds
    (define-key evil-normal-state-map (kbd "}") 'endless/forward-paragraph)
    (define-key evil-normal-state-map (kbd "{") 'endless/backward-paragraph)
    (evil-mode 1)))

(use-package evil-quickscope
  :ensure t
  :config
  (global-evil-quickscope-mode 1))

(use-package markdown-mode
  :ensure t
  :mode
  (("\\.md$" . markdown-mode)))

(use-package markdown-preview-mode
  :ensure t
  :init
  (setq markdown-command "multimarkdown"))

(use-package direx
  :ensure t)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

(use-package embrace
  :ensure t)

(use-package ledger-mode
  :ensure t)

(use-package magit
  :ensure t)

(use-package eink-theme
  :ensure t
  :config
  (load-theme 'eink 1))

(use-package aggressive-indent
  :ensure t
  :config
  (progn
    (add-hook 'emacs-lisp-mode-hook 'aggressive-indent-mode)
    (add-hook 'clojure-mode-hook 'aggressive-indent-mode)))

(use-package clojure-mode
  :ensure t)

(use-package cider
  :ensure t)

(use-package paren-face
  :ensure t
  :config
  (progn
    (add-hook 'emacs-lisp-mode-hook 'paren-face-mode)
    (add-hook 'clojure-mode-hook 'paren-face-mode)))

(use-package beacon
  :ensure t
  :init
  (setq beacon-blink-when-window-changes nil)
  :config
  (beacon-mode 1))

(use-package switch-window
  :ensure t)

(use-package shackle
  :ensure t
  :init
  (setq shackle-default-rule '(:select t
                                       :inhibit-window-quit nil
                                       :popup t)))

(use-package golden-ratio
  :ensure t
  :init
  (setq golden-ratio-extra-commands
        '(evil-window-left
          evil-window-right
          evil-window-up
          evil-window-down)
        golden-ratio-auto-scale 1)
  :config
  (golden-ratio-mode 1))

(use-package elixir-mode)

(use-package ivy
  :ensure t
  :config
  (ivy-mode 1))

(use-package projectile
  :ensure t
  :init
  (setq projectile-completion-system 'ivy))

(use-package avy
  :ensure t)

;; For use without Evil mode

;; (use-package swiper
;;   :ensure t)

;; (use-package expand-region
;;   :ensure t
;;   :bind ("C-=" . er/expand-region))

;; (use-package lispy
;;   :ensure t
;;   :config
;;   (progn
;;     (add-hook 'emacs-lisp-mode-hook 'lispy-mode)
;;     (add-hook 'clojure-mode-hook 'lispy-mode)))

(provide 'my-packages)
